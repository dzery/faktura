<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Form\{
    InvoiceType,
    SubjectType
};
use App\Entity\{
    Invoice,
    Subject
};
use App\Repository\InvoiceRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{
    Entity
};
use Symfony\Component\HttpFoundation\{
    Response,
    Request
};

/**
 * Controller of invoice.
 * 
 * @author Jarosław Kasica <jaroslaw.kasica@gmail.com>
 */
class InvoiceController extends AbstractController {

    /**
     * List of invoices.
     * 
     * @Route("/", name="invoice__index_page", methods={"GET"})
     */
    public function indexPage(InvoiceRepository $invoiceRepo): Response {

        return $this->render('invoice/index.html.twig', [
                    'found_invoices' => $invoiceRepo->findAll(),
        ]);
    }

    /**
     * Page when user can add new invoice.
     * 
     * @Route("/nowa", name="invoice__add_page", methods={"GET","POST"})
     */
    public function invoiceAddPage(Request $req): Response {

        $em = $this->getDoctrine()->getManager();

        $invoiceForm = $this->createForm(InvoiceType::class);
        $invoiceForm->handleRequest($req);

        if ($invoiceForm->isSubmitted() && $invoiceForm->isValid()) {

            $invoiceFromForm = $invoiceForm->getData();

            $em->persist($invoiceFromForm);
            $em->flush();

            return $this->redirectToRoute('invoice__edit_page', ['id' => $invoiceFromForm->getId()]);
        }

        return $this->render('invoice/add.html.twig', [
                    'invoice_form' => $invoiceForm->createView(),
        ]);
    }

    /**
     * Edit invoices by id.
     * 
     * @Route("/edycja,{id}", name="invoice__edit_page", methods={"GET","POST"})
     */
    public function invoiceEditPage(Request $req, Invoice $invoice): Response {

        $em = $this->getDoctrine()->getManager();

        $subjectForm = $this->createForm(SubjectType::class);
        $subjectForm->handleRequest($req);

        if ($subjectForm->isSubmitted() && $subjectForm->isValid()) {

            $subjectFromForm = $subjectForm->getData();

            $invoice
                    ->addSubject($subjectFromForm)
                    ->setSum($invoice->getSum() + $subjectFromForm->getPrice());

            $em->persist($subjectFromForm);
            $em->flush();

            return $this->redirectToRoute('invoice__edit_page', ['id' => $invoice->getId()]);
        }

        return $this->render('invoice/edit.html.twig', [
                    'invoice' => $invoice,
                    'subject_form' => $subjectForm->createView(),
        ]);
    }

    /**
     * Remove subject from invoice and updates price of invoice.
     * 
     * @Route("/towar/usun,{id}", name="invoice__subject_remove_page", methods={"GET"})
     */
    public function subjectRemoveRoute(Subject $subject): Response {

        $em = $this->getDoctrine()->getManager();

        $invoiceFromSubject = $subject->getInvoice();
        $invoiceFromSubject->setSum($invoiceFromSubject->getSum() - $subject->getPrice());

        $subject->setDeletedAt(new \DateTime);

        $em->persist($subject);
        $em->flush();

        return $this->redirectToRoute('invoice__edit_page', ['id' => $invoiceFromSubject->getId()]);
    }

}
