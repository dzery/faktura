<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Company;

class CompanyFixtures extends Fixture {

    public function load(ObjectManager $manager) {
        $company = (new Company())
                ->setCity('Testowa ulica')
                ->setHouseNo('007')
                ->setName('Testowa nazwa')
                ->setPostal('00-000')
                ->setStreet('testowa ulica');

        $manager->persist($company);

        $company2 = (new Company())
                ->setCity('Testowa ulica 2')
                ->setHouseNo('007 2')
                ->setName('Testowa nazwa 2')
                ->setPostal('00-000')
                ->setStreet('testowa ulica 2');

        $manager->persist($company2);

        $manager->flush();
    }

}
