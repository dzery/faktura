<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class Company {

    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $houseNo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postal;

    /**
     * @ORM\OneToMany(targetEntity="Invoice", mappedBy="dealer",cascade={"persist","remove"})
     */
    private $invoicesDeal;

    /**
     * @ORM\OneToMany(targetEntity="Invoice", mappedBy="buyer",cascade={"persist","remove"})
     */
    private $invoicesBuy;

    public function __construct() {
        $this->invoicesDeal = new ArrayCollection();
        $this->invoicesBuy = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return \self
     */
    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string {
        return $this->street;
    }

    /**
     * @param string $street
     * @return \self
     */
    public function setStreet(string $street): self {
        $this->street = $street;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHouseNo(): ?string {
        return $this->houseNo;
    }

    /**
     * @param string $houseNo
     * @return \self
     */
    public function setHouseNo(string $houseNo): self {
        $this->houseNo = $houseNo;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string {
        return $this->city;
    }

    /**
     * @param string $city
     * @return \self
     */
    public function setCity(string $city): self {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPostal(): ?string {
        return $this->postal;
    }

    /**
     * @param string $postal
     * @return \self
     */
    public function setPostal(string $postal): self {
        $this->postal = $postal;
        return $this;
    }

    public function getInvoicesDeal() {
        return $this->invoicesDeal;
    }

    public function setInvoicesDeal($invoicesDeal): self {
        $this->invoicesDeal = $invoicesDeal;
        return $this;
    }

    /**
     * @param \App\Entity\Invoice $invoice
     * @return \self
     */
    public function addInvoicesDeal(Invoice $invoice): self {
        $invoice->setDealer($this);
        $this->invoicesDeal[] = $invoice;
        return $this;
    }

    /**
     * @param \App\Entity\Invoice $invoice
     * @return \self
     */
    public function addInvoicesBuy(Invoice $invoice): self {
        $invoice->setBuyer($this);
        $this->invoicesBuy[] = $invoice;
        return $this;
    }

    public function getInvoicesBuy() {
        return $this->invoicesBuy;
    }

    public function setInvoicesBuy($invoicesBuy): self {
        $this->invoicesBuy = $invoicesBuy;
        return $this;
    }

}
