<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 * 
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class Invoice {

    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $sum = 0;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="invoicesDeal")
     * @ORM\JoinColumn(name="invoice_dealer_id",referencedColumnName="id",nullable=false, onDelete="CASCADE")
     */
    private $dealer;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="invoicesBuy")
     * @ORM\JoinColumn(name="invoice_buyer_id",referencedColumnName="id",nullable=false, onDelete="CASCADE")
     */
    private $buyer;

    /**
     * @ORM\OneToMany(targetEntity="Subject", mappedBy="invoice",cascade={"persist"})
     */
    private $subjects;

    public function __construct() {
        $this->subjects = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getSum(): ?int {
        return $this->sum;
    }

    /**
     * @param int $sum
     * @return \self
     */
    public function setSum(int $sum): self {
        $this->sum = $sum;
        return $this;
    }

    /**
     * @return \App\Entity\Company
     */
    public function getDealer(): ?Company {
        return $this->dealer;
    }

    /**
     * @param \App\Entity\Company $dealer
     * @return \self
     */
    public function setDealer(Company $dealer): self {
        $this->dealer = $dealer;
        return $this;
    }

    /**
     * @return \App\Entity\Company
     */
    public function getBuyer(): ?Company {
        return $this->buyer;
    }

    /**
     * @param \App\Entity\Company $buyer
     * @return \self
     */
    public function setBuyer(Company $buyer): self {
        $this->buyer = $buyer;
        return $this;
    }

    public function getSubjects() {
        return $this->subjects;
    }

    public function setSubjects($subjects): self {
        $this->subjects = $subjects;
        return $this;
    }

    /**
     * @param \App\Entity\Subject $subject
     * @return \self
     */
    public function addSubject(Subject $subject): self {
        $subject->setInvoice($this);
        $this->subjects[] = $subject;
        return $this;
    }

}
