<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 */
class Subject {

    use SoftDeleteableEntity;
    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank(      
     *                  groups={"subject_group"}
     * )
     * 
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Assert\NotBlank(      
     *                  groups={"subject_group"}
     * )
     *   
     * 
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @Assert\NotBlank(      
     *                  groups={"subject_group"}
     * )
     * 
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="Invoice", inversedBy="subjects")
     * @ORM\JoinColumn(name="invoice_id",referencedColumnName="id",nullable=false, onDelete="CASCADE")
     */
    private $invoice;

    /**
     * @return int|null
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @param string $name
     * @return \self
     */
    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPrice(): ?int {
        return $this->price;
    }

    /**
     * @param int $price
     * @return \self
     */
    public function setPrice(int $price): self {
        $this->price = $price;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return \self
     */
    public function setQuantity(int $quantity): self {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return \App\Entity\Invoice
     */
    public function getInvoice(): Invoice {
        return $this->invoice;
    }

    /**
     * @param \App\Entity\Invoice $invoice
     */
    public function setInvoice(Invoice $invoice): self {
        $this->invoice = $invoice;
        return $this;
    }

}
