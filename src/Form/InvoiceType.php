<?php

namespace App\Form;

use App\Entity\Invoice;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\{
    SubmitType
};

class InvoiceType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('dealer', EntityType::class, [
                    'label' => 'Sprzedawca',
                    'class' => \App\Entity\Company::class,
                    'choice_value' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('t')
                                ->orderBy('t.name', 'DESC');
                    },
                    'choice_label' => 'name',
                ])
                ->add('buyer', EntityType::class, [
                    'label' => 'Kupujący',
                    'class' => \App\Entity\Company::class,
                    'choice_value' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('c')
                                ->orderBy('c.name', 'DESC');
                    },
                    'choice_label' => 'name',
                ])
                ->add('save', SubmitType::class, [
                    'label' => 'Zapisz'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }

}
