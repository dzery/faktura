<?php

namespace App\Form;

use App\Entity\Subject;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\{
    SubmitType,
    TextType,
    IntegerType
};

class SubjectType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', TextType::class, [
                    'label' => 'Nazwa'
                ])
                ->add('price', IntegerType::class, [
                    'label' => 'Cena'
                ])
                ->add('quantity', IntegerType::class, [
                    'data' => '1',
                    'label' => 'Ilość'
                ])
                ->add('save', SubmitType::class, [
                    'label' => 'Dodaj nową pozycję'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Subject::class,
            'validation_groups' => 'subject_group'
        ]);
    }

}
